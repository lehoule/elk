package dev.cantcode.elk.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import co.elastic.apm.attach.ElasticApmAttacher;

@SpringBootApplication
public class SpringbootApplication {

	public static void main(String[] args) {

		ElasticApmAttacher.attach();
		SpringApplication.run(SpringbootApplication.class, args);
	}

}
