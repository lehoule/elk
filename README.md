# ELK For Docker

Testing the auto discovery setup of metricbeat and filebeat on Docker


## Filebeat and metricbeat autodiscover 
https://www.elastic.co/guide/en/beats/filebeat/current/running-on-docker.html
https://www.elastic.co/guide/en/beats/metricbeat/current/running-on-docker.html

## Spring Boot APM
https://www.elastic.co/guide/en/apm/agent/java/master/setup-attach-api.html

## Nginx Metrics
https://www.tecmint.com/enable-nginx-status-page/

## Develop

Copy the .env.template to .env
Add your Elastic cloud id and auth 

Rename the springboot/src/main/ressources/elasticapm.properties.template to elasticapm.properties
Add the missing values. Get the values by adding the apm integration on Elastic Cloud

run `docker-compose up --build`